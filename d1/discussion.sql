-- [SECTION] Add New Records

-- Add Artists
INSERT INTO artists (name) VALUES ("Taylor Swift");
INSERT INTO artists (name) VALUES ("Lady Gaga");
INSERT INTO artists (name) VALUES ("Justin Bieber");
INSERT INTO artists (name) VALUES ("Ariana Grande");
INSERT INTO artists (name) VALUES ("Bruno Mars");

-- Add Albums
INSERT INTO albums (album_title, date_released, artist_id) VALUES (
	"Fearless",
	"2008-01-01",
	3
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES (
	"Red",
	"2012-01-01",
	3
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES (
	"A Star Is Born",
	"2018-01-01",
	4
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES (
	"Born This Way",
	"2011-01-01",
	4
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES (
	"Purpose",
	"2015-01-01",
	5
);

INSERT INTO albums (album_title, date_released, artist_id) VALUES (
	"Dangerous Woman",
	"2016-01-01",
	6
);

-- Add Songs
INSERT INTO songs (song_name, length, genre, album_id) VALUES (
	"Fearless",
	246,
	"Pop rock",
	3
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
	"State of Grace",
	253,
	"Rock, alternative rock, arena rock",
	4
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
	"Black Eyes",
	151,
	"Rock",
	5
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
	"Born This Way",
	252,
	"Electropop",
	6
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
	"Sorry",
	212,
	"Dancehall",
	7
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES (
	"Into You",
	242,
	"EDM House",
	8
);

-- [SECTION] Advanced Selects

-- Exclude records
SELECT * FROM songs WHERE id != 5;

-- Greater than (or equal to)
SELECT * FROM songs WHERE id >= 4;

-- Less than (or equal to)
SELECT * FROM songs WHERE id <= 7;

-- Get specific IDs (OR)
SELECT * FROM songs WHERE id = 1 OR id = 5 OR id = 6;

-- Get specific IDs (IN)
SELECT * FROM songs WHERE id IN (1, 5, 6);

-- Find partial matches
SELECT * FROM songs WHERE song_name LIKE "%e"; --select keyword from the end
SELECT * FROM songs WHERE song_name LIKE "b%"; --select keyword from the start
SELECT * FROM songs WHERE song_name LIKE "%a%"; --select keyword inbetween

-- Sort records
SELECT * FROM songs ORDER BY song_name ASC;
SELECT * FROM songs ORDER BY song_name DESC;

-- Limit returned records
SELECT * FROM songs ORDER BY song_name DESC LIMIT 3;

-- Offset/skip records
SELECT * FROM songs LIMIT 5 OFFSET 2;

-- Getting distinct records (show all unique values)
SELECT DISTINCT genre FROM songs;

-- Count
SELECT COUNT(*) FROM songs WHERE genre = "Dancehall";

--[SECTION] Table Joins

--Combine artists and ALbums table (INNER JOIN)
SELECT * FROM artists 
	JOIN albums ON artists.id = albums.artist_id;

-- Left join (show all srtist regardless of album status)
SELECT * FROM artists
	LEFT JOIN  albums ON artist.id = albums.artist_id;

-- Right join (show all srtist regardless of album status)
SELECT * FROM artists
	RIGHT JOIN  albums ON artist.id = albums.artist_id;


-- Mini activity:
-- In a single SELECT command, show all artist with thier corresponding albums ans songs

SELECT * FROM songs JOIN albums ON songs.album_id = albums.id JOIN artists ON albums.artist_id = artists.id;